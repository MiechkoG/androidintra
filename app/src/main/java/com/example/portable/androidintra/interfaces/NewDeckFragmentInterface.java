package com.example.portable.androidintra.interfaces;

/**
 * Created by Vlad on 11/17/2016.
 */

public interface NewDeckFragmentInterface {
    public void onSettingConfirmed(String deckName);
}
