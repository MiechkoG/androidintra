package com.example.portable.androidintra.interfaces;

/**
 * Created by Vlad on 11/22/2016.
 */

public interface CardPickerInterface {
    public void onCardAdded(String cardName, int amount);
}
