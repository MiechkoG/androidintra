package com.example.portable.androidintra.core;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.portable.androidintra.Fragments.CardPickerDialog;
import com.example.portable.androidintra.R;
import com.example.portable.androidintra.bean.Card;
import com.example.portable.androidintra.bean.Deck;
import com.example.portable.androidintra.adapters.DeckDetailsAdapter;
import com.example.portable.androidintra.interfaces.CardPickerInterface;
import com.example.portable.androidintra.utils.Constants;
import com.example.portable.androidintra.utils.DAO;

/**
 * Created by Vlad on 11/17/2016.
 */

public class DeckDetails extends AppCompatActivity implements CardPickerInterface{
    private Deck deck;
    private ListView listView;
    private TextView deckName;
    private DeckDetailsAdapter deckDetailsAdapter;
    private DAO dao;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.deck_details);
        dao = new DAO(this);

        deck = (Deck)getIntent().getExtras().getSerializable("deck");

        listView = (ListView) findViewById(R.id.deckDetailsListView);
        deckName = (TextView)findViewById(R.id.deckDetailsName);

        deckName.setText(deck.getName());

        deckDetailsAdapter  = new DeckDetailsAdapter(getBaseContext(), deck, this);
        listView.setAdapter(deckDetailsAdapter);
    }

    public void addCard(View v){
        initDialogAddCard();
    }

    public void removeCard(int cardId){
        dao.deleteCardByIdAndDeckId(cardId, deck.getId());
        refreshDeckList();
    }

    @Override
    public void onCardAdded(String cardName, int amount) {
        boolean cardFound = false;
        for(Card c : Constants.cards){
            if(c.getName().equalsIgnoreCase(cardName)){
                dao.insertCardInDeck(c, amount, deck.getId());
                refreshDeckList();
                cardFound = true;
                break;
            }
        }

        if(!cardFound){
            Toast.makeText(this, "Card : '" + cardName + "' does not exist", Toast.LENGTH_LONG).show();
        }
    }

    private void refreshDeckList() {
        deck = dao.loadDeckById(deck.getId());
        deckDetailsAdapter.setDeck(deck);
        deckDetailsAdapter.notifyDataSetChanged();
    }

    private void initDialogAddCard(){
        String[] names = new String[Constants.cardNames.size()];
        names = Constants.cardNames.toArray(names);
        FragmentManager fragmentManager = getSupportFragmentManager();
        CardPickerDialog dialog = new CardPickerDialog();
        dialog.attach(this, names);
        dialog.show(fragmentManager, "DialogSettings");
    }
}
