package com.example.portable.androidintra.utils;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import com.example.portable.androidintra.bean.Card;
import com.example.portable.androidintra.bean.CardDB;
import com.example.portable.androidintra.bean.Deck;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vlad on 11/17/2016.
 */

public class DAO extends SQLiteOpenHelper {
    SQLiteDatabase database;
    Context context;
    public DAO(Context context){
        super(context, Constants.DATABASE_NAME, null, Constants.DATABASE_VERSION);
        this.database = this.getWritableDatabase();
        this.context = context;
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Constants.SQL_CREATE_TABLE_DECKLISTS);
        db.execSQL(Constants.SQL_CREATE_TABLE_CARDS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public List<Integer> getDeckIds(){
        Cursor cursor = database.rawQuery(Constants.SQL_GET_DECK_ID, null);
        List<Integer> result = new ArrayList<Integer>();
        while(cursor.moveToNext()){
            result.add(cursor.getInt(0));
        }
        return result;
    }

    public void insertNewDeck(int deckId, String deckName){
        String[] args = {Integer.toString(deckId), deckName};
        database.execSQL(Constants.SQL_INSERT_NEW_DECK, args);
        Toast.makeText(context, "Deck Created", Toast.LENGTH_SHORT).show();
    }

    public void insertCardInDeck(Card card, int amount, int deckId){
        Log.d("SQL QUERY", "insertCardInDeck");
        String name = card.getName();
        if(loadCardByNameAndDeckId(name, deckId) == null) {
            String[] s = {Integer.toString(card.getMultiverseId()), Integer.toString(amount), name, Integer.toString(deckId)};
            database.execSQL(Constants.SQL_INSERT_CARD_IN_DECK, s);
        }else{
            changeAmountOfCard(card, amount, deckId);
        }
    }

    public void changeAmountOfCard(Card card, int amount, int deckId){
        Log.d("SQL QUERY", "changeAmountOfCard");
        int originalCardAmount = loadCardAmountByIdAndDeckId(card.getMultiverseId(), deckId);
        int finalAmount = originalCardAmount + amount;

        String[] args = {Integer.toString(finalAmount), Integer.toString(card.getMultiverseId()), Integer.toString(deckId)};
        database.execSQL(Constants.SQL_UPDATE_CARD_AMOUNT_BY_ID_AND_DECK_ID, args);
    }

    public int loadCardAmountByIdAndDeckId(int cardId, int deckId){
        int amount = 0;
        String[] args = {Integer.toString(cardId), Integer.toString(deckId)};
        Cursor cursor  = database.rawQuery(Constants.SQL_FETCH_CARD_AMOUNT_BY_ID_AND_DECK_ID, args);
        while(cursor.moveToNext()){
            amount = cursor.getInt(0);
        }
        return amount;
    }

    public List<Deck> loadAllDecks(){
        Log.d("SQL QUERY", "loadAllDeckss");
        List<Deck> decks = new ArrayList<Deck>();
        Cursor cursor = database.rawQuery(Constants.SQL_FETCH_ALL_DECKS, null);
        while(cursor.moveToNext()){
            Deck d = new Deck();
            int id = cursor.getInt(cursor.getColumnIndex("deckId"));
            String name = cursor.getString(cursor.getColumnIndex("name"));
            d.setId(id);
            d.setName(name);
            List<CardDB> cards = loadCardsByDeckId(id);
            d.setCards(cards);
            decks.add(d);
        }
        return decks;
    }

    public List<CardDB> loadCardsByDeckId(int deckId){
        Log.d("SQL QUERY", "loadCardsByDeckId");
        List<CardDB> cards = new ArrayList<CardDB>();
        String[] args = {Integer.toString(deckId)};
        Cursor cursor = database.rawQuery(Constants.SQL_FETCH_CARDS_BY_DECK_ID , args);
        while(cursor.moveToNext()){
            CardDB c = new CardDB();
            int id = cursor.getInt(cursor.getColumnIndex("cardId"));
            int amount = cursor.getInt(cursor.getColumnIndex("amount"));
            String name = cursor.getString(cursor.getColumnIndex("name"));
            c.setName(name);
            c.setAmount(amount);
            c.setMultiverseId(id);
            cards.add(c);
        }
        return cards;
    }

    public CardDB loadCardByNameAndDeckId(String name, int deckId){
        CardDB foundCard = null;
        String[] args = {name, Integer.toString(deckId)};
        Cursor cursor = database.rawQuery(Constants.SQL_FETCH_CARDS_BY_NAME_AND_DECK_ID, args);
        while(cursor.moveToNext()){
            int multiverseId = cursor.getInt(cursor.getColumnIndex("cardId"));
            int amount = cursor.getInt(cursor.getColumnIndex("amount"));
            foundCard = new CardDB();
            foundCard.setMultiverseId(multiverseId);
            foundCard.setAmount(amount);
            foundCard.setName(name);
        }
        return foundCard;
    }
    public Deck loadDeckById(int deckId){
        Log.d("SQL QUERY", "loadDeckById");
        Deck deck = new Deck();
        String[] args = {Integer.toString(deckId)};
        Cursor cursor = database.rawQuery(Constants.SQL_FETCH_DECK_BY_ID, args);
        while(cursor.moveToNext()){
            deck.setId(cursor.getInt(cursor.getColumnIndex("deckId")));
            deck.setName(cursor.getString(cursor.getColumnIndex("name")));
            deck.setCards(loadCardsByDeckId(deckId));
        }
        return deck;
    }

    public void deleteDeckById(int deckId){
        String[] args = {Integer.toString(deckId)};
        database.execSQL(Constants.SQL_DELETE_DECK_BY_ID, args);
    }

    public void deleteCardByIdAndDeckId(int cardId, int deckId) {
        String[] args = {Integer.toString(deckId), Integer.toString(cardId)};
        database.execSQL(Constants.SQL_DELETE_CARDS_BY_ID_AND_DECK_ID, args);
    }

}
