package com.example.portable.androidintra.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Vlad on 11/17/2016.
 */

public class Deck implements Serializable{
    private int id;
    private String name;
    private List<CardDB> cards;

    public Deck(){
        cards = new ArrayList<CardDB>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CardDB> getCards() {
        return cards;
    }

    public void setCards(List<CardDB> cards) {
        this.cards = cards;
    }

    public void addCard(CardDB c){
        cards.add(c);
    }

    public void removeCard(int cardId){
        for(CardDB c : cards){
            if(c.getMultiverseId() == cardId){
                c.setAmount(c.getAmount()-1);
            }
        }
    }
}
