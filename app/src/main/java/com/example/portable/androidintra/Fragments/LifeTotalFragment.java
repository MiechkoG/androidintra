package com.example.portable.androidintra.Fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.portable.androidintra.R;

/**
 * Created by Miechko Gibson Wyjadlowski on 2016-11-10.
 */

public class LifeTotalFragment extends Fragment {
    private int currentHp;
    private TextView lifeTotal;
    private TextView addLife;
    private TextView addFiveLife;
    private TextView loseLife;
    private TextView loseFiveLife;
    private LinearLayout mainLayout;
    private Bundle args;
    View v;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.lifetotal_fragment, container, false);

        lifeTotal = (TextView) v.findViewById(R.id.playerLifeTextView);
        mainLayout = (LinearLayout) v.findViewById(R.id.mainLayout);

        args = getArguments();
        if(args != null){
            currentHp = args.getInt("currentHp");
            lifeTotal.setText(currentHp +"");
        }

        initializeOnClickListeners();

        return v;
    }

    private void initializeOnClickListeners() {
        addLife = (TextView) v.findViewById(R.id.plusHpButton);
        addFiveLife = (TextView) v.findViewById(R.id.plus5HpButton);
        loseLife = (TextView) v.findViewById(R.id.minusHpButton);
        loseFiveLife = (TextView) v.findViewById(R.id.minus5HpButton);

        addLife.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentHp ++;
                lifeTotal.setText(currentHp+"");
                if(currentHp > 0){
                    mainLayout.setBackgroundColor(Color.rgb(255, 255, 255));
                }
            }
        });

        addFiveLife.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentHp += 5;
                lifeTotal.setText(currentHp+"");
                if(currentHp > 0){
                    mainLayout.setBackgroundColor(Color.rgb(255, 255, 255));
                }
            }
        });

        loseLife.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentHp --;
                lifeTotal.setText(currentHp+"");
                if(currentHp <= 0){
                    mainLayout.setBackgroundColor(Color.rgb(246, 88, 88));
                }
            }
        });

        loseFiveLife.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentHp -= 5;
                lifeTotal.setText(currentHp+"");
                if(currentHp <= 0){
                    mainLayout.setBackgroundColor(Color.rgb(246, 88, 88));
                }
            }
        });
    }
}
