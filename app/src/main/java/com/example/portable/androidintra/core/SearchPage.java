package com.example.portable.androidintra.core;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.portable.androidintra.R;
import com.example.portable.androidintra.bean.Card;
import com.example.portable.androidintra.interfaces.FileLoaderInterface;
import com.example.portable.androidintra.utils.Constants;
import com.example.portable.androidintra.utils.FileDownloader;
import com.example.portable.androidintra.utils.FileLoader;
import com.example.portable.androidintra.utils.ImageFetcher;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import java.io.File;

/**
 * Created by Vlad on 11/13/2016.
 */

public class SearchPage extends AppCompatActivity implements FileDownloader.FileDownloaderInterface, ImageFetcher.ImageFetcherInterface, FileLoaderInterface {
    private AutoCompleteTextView nameSearchTextView;
    private TextView cardName;
    private TextView cardText;
    private TextView cardType;
    private ImageView cardImage;
    private Context context;
    private File path;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_page);

        context = getApplicationContext();
        path = context.getFilesDir();
        nameSearchTextView = (AutoCompleteTextView) findViewById(R.id.searchView);
        cardName = (TextView) findViewById(R.id.cardName);
        cardText = (TextView) findViewById(R.id.cardText);
        cardType = (TextView) findViewById(R.id.cardType);
        cardImage = (ImageView) findViewById(R.id.cardImage);

        setImageViewSize();

        FileLoader fileLoader = new FileLoader(this, getFilesDir().getPath());
        fileLoader.execute();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.search_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_updateDB:
                findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
                findViewById(R.id.searchView).setEnabled(false);
                String savePath = getFilesDir().getPath();
                FileDownloader fileDownloader = new FileDownloader(this, savePath);
                fileDownloader.execute();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResponse(String message) {
        if (message.equalsIgnoreCase(Constants.OPERATION_FILE_LOADED)) {
            setupAutocompleteAdapter();
        } else if (message.equalsIgnoreCase(Constants.OPERATION_FILE_DOWNLOADED)) {
            findViewById(R.id.progressBar).setVisibility(View.GONE);
            findViewById(R.id.searchView).setEnabled(true);
            setupAutocompleteAdapter();
        }
    }

    @Override
    public void onResponse(Bitmap bitmap) {
        cardImage.setImageBitmap(bitmap);
    }

    protected void searchCard(View view) {
        String searchedName = nameSearchTextView.getText().toString();
        Card searchedCard = null;

        if (!searchedName.equalsIgnoreCase("")) {
            for (Card c : Constants.cards) {
                if (c.getName().equalsIgnoreCase(searchedName)) {
                    searchedCard = c;
                    break;
                }
            }
            if (searchedCard != null) {
                cardName.setText(searchedCard.getName());
                cardType.setText(searchedCard.getType());
                cardText.setText(searchedCard.getText());
                String imageUrl = Constants.IMAGE_DOWNLOAD_FILE_URL + searchedCard.getMultiverseId();
                fetchImage(imageUrl);
            } else {
                cardName.setText("Card not Found");
            }
        }
    }

    private void fetchImage(String imageUrl) {
        ImageFetcher imageFetcher = new ImageFetcher(this, imageUrl);
        imageFetcher.execute();
    }

    private void setupAutocompleteAdapter() {
        Constants.fillCardNamesList();
        String[] names = new String[Constants.cardNames.size()];
        names = Constants.cardNames.toArray(names);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, names);
        nameSearchTextView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void setImageViewSize() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        int calculatedSize;
        if (height < width) {
            calculatedSize = width / 2;
        } else {
            calculatedSize = height / 2;
        }

        cardImage.getLayoutParams().height = calculatedSize;
        cardImage.getLayoutParams().width = calculatedSize;
    }
}

