package com.example.portable.androidintra.core;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.TextView;

import com.example.portable.androidintra.R;
import com.example.portable.androidintra.interfaces.FileLoaderInterface;
import com.example.portable.androidintra.utils.Constants;
import com.example.portable.androidintra.utils.FileLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Miechko Gibson Wyjadlowski on 2016-11-10.
 */

public class MainMenu extends AppCompatActivity implements FileLoaderInterface{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu);

        FileLoader fileLoader = new FileLoader(this, getFilesDir().getPath());
        fileLoader.execute();

        setSizeOptions();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    protected void rollDice(View view) {
        int result = (int) (Math.random() * 100) + 1;
        TextView textView = (TextView) findViewById(R.id.diceRollResult);
        textView.setText(Integer.toString(result));
    }

    protected void launchLifeCounterPage(View v){
        Intent intent = new Intent(this, LifeCounterPage.class);
        startActivity(intent);
    }

    protected void launchSearchPage(View v){
        Intent intent = new Intent(this, SearchPage.class);
        startActivity(intent);
    }
    protected void launchDecklistsPage(View v){
        Intent intent = new Intent(this, DecklistsPage.class);
        startActivity(intent);
    }

    @Override
    public void onResponse(String message) {
        Constants.fillCardNamesList();
    }

    private void setSizeOptions(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        int deviceHeight = displayMetrics.heightPixels;
        int deviceWidth = displayMetrics.widthPixels;

        List<Button> buttons = new ArrayList<Button>();
        buttons.add((Button) findViewById(R.id.lifeCounterButton));
        buttons.add((Button) findViewById(R.id.searchCardButton));
        buttons.add((Button) findViewById(R.id.decklistsButton));
        buttons.add((Button) findViewById(R.id.rollDiceButton));

        int size = 0;
        if(deviceHeight >= deviceWidth){
            size = (deviceHeight - 20) / 4;
        } else{
            size = (deviceWidth / 4 ) - 30;
        }

        for(Button b : buttons){
            b.setHeight(size);
            b.setWidth(size);
        }
    }
}
