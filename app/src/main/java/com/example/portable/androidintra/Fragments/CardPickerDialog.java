package com.example.portable.androidintra.Fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import com.example.portable.androidintra.R;
import com.example.portable.androidintra.interfaces.CardPickerInterface;

/**
 * Created by Vlad on 11/22/2016.
 */

public class CardPickerDialog extends DialogFragment{
    private CardPickerInterface cardPickerInterface;
    private AutoCompleteTextView cardNameTextView;
    private EditText cardAmountView;
    private String[] names;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final View v = LayoutInflater.from(getActivity()).inflate(R.layout.card_picker_fragment, null);
        cardNameTextView = (AutoCompleteTextView) v.findViewById(R.id.cardPicker);
        cardAmountView = (EditText) v.findViewById(R.id.amountPicker);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_expandable_list_item_1, names);
        cardNameTextView.setAdapter(adapter);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String cardName = cardNameTextView.getText().toString();
                if(!cardName.equalsIgnoreCase("")) {
                    int amount = Integer.valueOf(cardAmountView.getText().toString());
                    cardPickerInterface.onCardAdded(cardName, amount);
                }
            }
        });
        builder.setView(v);
        return builder.create();
    }

    public void attach(CardPickerInterface cardPickerInterface, String[] names){
        this.cardPickerInterface = cardPickerInterface;
        this.names = names;
    }
}
