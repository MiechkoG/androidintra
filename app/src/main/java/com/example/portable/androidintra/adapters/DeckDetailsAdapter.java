package com.example.portable.androidintra.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.example.portable.androidintra.R;
import com.example.portable.androidintra.bean.Card;
import com.example.portable.androidintra.bean.CardDB;
import com.example.portable.androidintra.bean.Deck;
import com.example.portable.androidintra.core.DeckDetails;
import com.example.portable.androidintra.utils.DAO;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by Vlad on 11/21/2016.
 */

public class DeckDetailsAdapter extends ArrayAdapter{
    private Deck deck;
    private Context context;
    private List<CardDB> cards;
    private DeckDetails deckDetails;

    public DeckDetailsAdapter(Context context, Deck deck, DeckDetails deckDetails){
        super(context, R.layout.deck_detail_list_item);
        this.context = context;
        this.deck = deck;
        this.cards = deck.getCards();
        this.deckDetails = deckDetails;
    }

    @Override
    public int getCount() {
        return cards.size();
    }

    @Nullable
    @Override
    public CardDB getItem(int position) {
        return cards.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, final View convertView, ViewGroup parent) {
        View v = convertView;
        final CardDB card = getItem(position);
        if(v == null){
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = layoutInflater.inflate(R.layout.deck_detail_list_item, parent, false);
        }

        TextView cardName = (TextView) v.findViewById(R.id.cardName);
        Button buttonDeleteCard = (Button) v.findViewById(R.id.buttonDeleteCard);

        cardName.setText(card.getAmount() + "x  " + card.getName());
        buttonDeleteCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               deckDetails.removeCard(card.getMultiverseId());
            }
        });
        return v;
    }

    public void setDeck(Deck d){
        this.deck = d;
        this.cards = d.getCards();
    }

}
