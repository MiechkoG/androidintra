package com.example.portable.androidintra.bean;

import java.io.Serializable;

/**
 * Created by Vlad on 11/17/2016.
 */

public class CardDB implements Serializable {
    private String name;
    private int multiverseId;
    private int amount;

    public CardDB() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMultiverseId() {
        return multiverseId;
    }

    public void setMultiverseId(int multiverseId) {
        this.multiverseId = multiverseId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
