package com.example.portable.androidintra.interfaces;

/**
 * Created by Vlad on 11/13/2016.
 */

public interface LifeCounterSettingsFragmentInterface {
    public void onSettingConfirmed(int startingLife, int nbOfPlayers);
}
