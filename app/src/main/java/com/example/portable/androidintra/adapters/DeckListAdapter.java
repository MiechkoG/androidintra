package com.example.portable.androidintra.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.portable.androidintra.R;
import com.example.portable.androidintra.bean.Deck;

import java.util.List;

/**
 * Created by Vlad on 11/17/2016.
 */

public class DeckListAdapter extends ArrayAdapter {
    Context context;
    List<Deck> decks;
    public DeckListAdapter(Context context, List<Deck> decks){
        super(context, R.layout.deck_list_item);
        this.context = context;
        this.decks = decks;
    }

    @Override
    public int getCount() {
        return decks.size();
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return decks.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        Deck deck = (Deck) getItem(position);
        if(v == null){
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = layoutInflater.inflate(R.layout.deck_list_item, parent, false);
        }

        TextView deckName = (TextView) v.findViewById(R.id.deckName);
        Button buttonModify = (Button) v.findViewById(R.id.buttonModify);
        Button buttonDelete = (Button) v.findViewById(R.id.buttonDelete);

        deckName.setText(deck.getName());
        buttonModify.setTag(deck.getId());
        buttonDelete.setTag(deck.getId());

        return v;
    }
    public void setDecks(List<Deck> decks) {
        this.decks = decks;
    }
    public void updateList(List<Deck> decks){
        this.decks = decks;
        notifyDataSetChanged();
    }
}
