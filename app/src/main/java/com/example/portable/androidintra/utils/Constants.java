package com.example.portable.androidintra.utils;

import com.example.portable.androidintra.bean.Card;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Vlad on 11/13/2016.
 */

public class Constants {
    public static Set<String> cardNames =  new HashSet<>();
    public static List<Card> cards = new ArrayList<>();

    public static void fillCardNamesList(){
        for(Card c : Constants.cards){
            Constants.cardNames.add(c.getName());
        }
    }

    public static final String DOWNLOAD_SOURCE_URL = "https://mtgjson.com/json/AllSets.json";
    public static final String LOCALLY_SAVED_CARD_DB_FILENAME = "/AllSets.json";
    public static final String IMAGE_DOWNLOAD_FILE_URL = "http://gatherer.wizards.com/Handlers/Image.ashx?type=card&multiverseid=";

    public static final String OPERATION_FILE_LOADED = "fileloaded";
    public static final String OPERATION_FILE_DOWNLOADED = "filedownloaded";


    public static final String DATABASE_NAME = "Decklists";
    public static final int DATABASE_VERSION = 1;

    public static final String SQL_CREATE_TABLE_DECKLISTS = "CREATE TABLE IF NOT EXISTS decks" +
                                                                "(deckId INTEGER PRIMARY KEY, name VARCHAR)";

    public static final String SQL_CREATE_TABLE_CARDS = "CREATE TABLE IF NOT EXISTS cards" +
                                                 "(cardId INTEGER PRIMARY KEY, amount INTEGER, name VARCHAR, deck INTEGER, FOREIGN KEY(deck) REFERENCES decks(deckId))";

    public static final String SQL_GET_DECK_ID = "SELECT deckId FROM  decks";

    public static final String SQL_INSERT_NEW_DECK = "INSERT INTO decks (deckId, name) VALUES( ? , ?) ";
    public static final String SQL_INSERT_CARD_IN_DECK = "INSERT INTO cards (cardId, amount, name, deck) VALUES( ?, ?, ?, ?)";

    public static final String SQL_FETCH_ALL_DECKS = "SELECT * FROM decks";
    public static final String SQL_FETCH_DECK_BY_ID = "SELECT * FROM decks WHERE deckId = ? ";
    public static final String SQL_FETCH_CARDS_BY_DECK_ID = "SELECT * FROM cards WHERE deck = ? ";
    public static final String SQL_FETCH_CARDS_BY_NAME_AND_DECK_ID = "SELECT * FROM cards WHERE name = ? AND deck = ?";
    public static final String SQL_UPDATE_CARD_AMOUNT_BY_ID_AND_DECK_ID = "UPDATE cards SET amount = ? WHERE cardId = ? AND deck = ? ";

    public static final String SQL_FETCH_CARD_AMOUNT_BY_ID_AND_DECK_ID = "SELECT amount FROM cards WHERE cardId = ? AND deck = ?";

    public static final String SQL_DELETE_DECK_BY_ID = "DELETE FROM decks WHERE deckId = ?";
    public static final String SQL_DELETE_CARDS_BY_ID_AND_DECK_ID = "DELETE FROM cards WHERE deck= ? AND cardId= ?";
}
