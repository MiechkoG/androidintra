package com.example.portable.androidintra.Fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.portable.androidintra.R;
import com.example.portable.androidintra.interfaces.LifeCounterSettingsFragmentInterface;

/**
 * Created by Vlad on 11/13/2016.
 */

public class LifeCounterSettingsFragment extends DialogFragment {
    LifeCounterSettingsFragmentInterface lifeCounterClient;
    TextView startingLifeView;
    TextView nbPlayersView;
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final View v = LayoutInflater.from(getActivity()).inflate(R.layout.life_counter_settings_fragment, null);
        startingLifeView = (TextView) v.findViewById(R.id.lifeTotalValue);
        nbPlayersView = (TextView) v.findViewById(R.id.amountOfPlayersValue);

        return new AlertDialog.Builder(getActivity()).setTitle("Settings").setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String lifeTotalPicked = startingLifeView.getText().toString();
                String nbPlayersPicked = LifeCounterSettingsFragment.this.nbPlayersView.getText().toString();
                lifeCounterClient.onSettingConfirmed(Integer.valueOf(lifeTotalPicked) , Integer.valueOf(nbPlayersPicked));
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        }).setView(v).create();
    }

    public void attach(LifeCounterSettingsFragmentInterface client){
        lifeCounterClient = client;
    }
}
