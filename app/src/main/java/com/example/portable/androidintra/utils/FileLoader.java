package com.example.portable.androidintra.utils;

import android.os.AsyncTask;

import com.example.portable.androidintra.bean.Card;
import com.example.portable.androidintra.interfaces.FileLoaderInterface;
import com.google.gson.stream.JsonReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by Vlad on 11/16/2016.
 */

public class FileLoader extends AsyncTask<Void, Void, String>{
    FileLoaderInterface fileLoaderInterface;
    String savePath;



    public FileLoader(FileLoaderInterface fileLoaderInterface, String savePath){
        this.savePath = savePath;
        this.fileLoaderInterface = fileLoaderInterface;
    }

    public void loadFile() {
        Constants.cards = new ArrayList<Card>();
        try {
            File file = new File(savePath+Constants.LOCALLY_SAVED_CARD_DB_FILENAME);
            String value = "";
            if(file.exists()){
                InputStream in = new FileInputStream(file);
                JsonReader reader = new JsonReader(new InputStreamReader(in));
                reader.beginObject();
                //ALLSETS
                while(reader.hasNext()){
                    reader.nextName();

                    //SET
                    reader.beginObject();
                    while (reader.hasNext()) {
                        value = reader.nextName();

                        //CARDS OF SET
                        if (value.equalsIgnoreCase("cards")) {
                            reader.beginArray();

                            //INDIVIDUAL CARDS
                            while(reader.hasNext()){
                                reader.beginObject();
                                Card c = new Card();
                                while (reader.hasNext()){
                                    value = reader.nextName();
                                    if(value.equalsIgnoreCase("name")){
                                        c.setName(reader.nextString());
                                    }
                                    else if(value.equalsIgnoreCase("type")) {
                                        c.setType(reader.nextString());
                                    }
                                    else if(value.equalsIgnoreCase("text")){
                                        c.setText(reader.nextString());
                                    }
                                    else if(value.equalsIgnoreCase("multiverseid")){
                                        c.setMultiverseId(reader.nextInt());
                                    }else{
                                        reader.skipValue();
                                    }
                                }
                                Constants.cards.add(c);

                                reader.endObject();
                            }

                            reader.endArray();
                        }else{
                            reader.skipValue();
                        }
                    }
                    reader.endObject();
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected String doInBackground(Void... params) {
        loadFile();
        return Constants.OPERATION_FILE_LOADED;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        fileLoaderInterface.onResponse(s);
    }
}
