package com.example.portable.androidintra.utils;

import android.os.AsyncTask;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * Created by Vlad on 11/13/2016.
 */

public class FileDownloader extends AsyncTask<Void, Void, String>{
    FileDownloaderInterface fileDownloaderInterface;
    String savePath;

    public FileDownloader(FileDownloaderInterface fileDownloaderInterface, String savePath){
        this.fileDownloaderInterface = fileDownloaderInterface;
        this.savePath = savePath;
    }
    public interface FileDownloaderInterface{
        void onResponse(String message);
    }

    private void download(){
        try{
            URL url = new URL(Constants.DOWNLOAD_SOURCE_URL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            if(connection.getResponseCode() == HttpURLConnection.HTTP_OK){
                InputStream inputStream = connection.getInputStream();

                String saveFilePath = savePath + Constants.LOCALLY_SAVED_CARD_DB_FILENAME;
                FileOutputStream outputStream = new FileOutputStream(saveFilePath);

                int bytesRead = -1;
                byte[] buffer = new byte[1024];
                while((bytesRead = inputStream.read(buffer)) != -1){
                    outputStream.write(buffer, 0 , bytesRead);
                }

                outputStream.close();
                inputStream.close();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    @Override
    protected String doInBackground(Void... params){
        download();
        return Constants.OPERATION_FILE_DOWNLOADED;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        fileDownloaderInterface.onResponse(s);
    }
}
