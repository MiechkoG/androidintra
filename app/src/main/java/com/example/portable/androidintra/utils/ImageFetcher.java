package com.example.portable.androidintra.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * Created by Vlad on 11/13/2016.
 */

public class ImageFetcher extends AsyncTask<Void, Void, Bitmap>{
    ImageFetcherInterface imageFetcherInterface;
    String url;
    public ImageFetcher(ImageFetcherInterface imageFetcherInterface, String url){
        this.imageFetcherInterface = imageFetcherInterface;
        this.url = url;
    }

    public interface ImageFetcherInterface{
        public void onResponse(Bitmap bitmap);
    }

    private Bitmap getImage() throws IOException{
        Bitmap bitmap = null;
        URL urlImage = new URL(url);
        InputStream in = (InputStream)urlImage.getContent();
        bitmap = BitmapFactory.decodeStream(in);
        return bitmap;
    }

    @Override
    protected Bitmap doInBackground(Void... params) {
        Bitmap bitmap = null;
        try{
            bitmap = getImage();
        } catch (Exception e){
            e.printStackTrace();
        }
        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        imageFetcherInterface.onResponse(bitmap);
    }
}
