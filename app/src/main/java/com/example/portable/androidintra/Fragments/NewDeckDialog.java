package com.example.portable.androidintra.Fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.portable.androidintra.R;
import com.example.portable.androidintra.interfaces.NewDeckFragmentInterface;

import org.w3c.dom.Text;

/**
 * Created by Vlad on 11/17/2016.
 */

public class NewDeckDialog extends DialogFragment {
    NewDeckFragmentInterface newDeckFragmentInterface;
    TextView deckName;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final View v = LayoutInflater.from(getActivity()).inflate(R.layout.new_deck_fragment, null);
        deckName = (TextView)v.findViewById(R.id.nameOfNewDeck);

        return new AlertDialog.Builder(getActivity()).setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String namePicked = deckName.getText().toString();
                newDeckFragmentInterface.onSettingConfirmed(namePicked);
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).setView(v).create();
    }

    public void attach(NewDeckFragmentInterface client){
        newDeckFragmentInterface = client;
    }
}
