package com.example.portable.androidintra.core;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.example.portable.androidintra.Fragments.LifeCounterSettingsFragment;
import com.example.portable.androidintra.Fragments.LifeTotalFragment;
import com.example.portable.androidintra.R;
import com.example.portable.androidintra.interfaces.LifeCounterSettingsFragmentInterface;

import java.util.ArrayList;
import java.util.List;

public class LifeCounterPage extends AppCompatActivity implements LifeCounterSettingsFragmentInterface{
    private static int numberOfPlayers = 2;
    private int startingHp = 20;
    private LinearLayout lifeTotalsView;
    private List<LifeTotalFragment> lifeTotalFragmentList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.life_counter);

        lifeTotalsView = (LinearLayout) findViewById(R.id.lifeTotalsView);

        Bundle args = getIntent().getExtras();
        if(args != null){
            numberOfPlayers = args.getInt("nbOfPlayers");
            startingHp = args.getInt("startingLife");
        }

        fillLifeTotalList();
    }

    private void fillLifeTotalList() {
        lifeTotalFragmentList = new ArrayList<LifeTotalFragment>();
        for(int i = 0; i < numberOfPlayers; i++){
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

            LifeTotalFragment f = setupNewFragment();

            ft.add(R.id.lifeTotalsView, f);
            ft.commit();
        }
    }

    @NonNull
    private LifeTotalFragment setupNewFragment() {
        LifeTotalFragment f = new LifeTotalFragment();
        Bundle args = new Bundle();
        args.putInt("currentHp", startingHp);
        f.setArguments(args);
        lifeTotalFragmentList.add(f);
        return f;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.life_counter_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_settings:
                showSettings();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void showSettings(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        LifeCounterSettingsFragment dialog = new LifeCounterSettingsFragment();
        dialog.attach(this);
        dialog.show(fragmentManager, "DialogSettings");
    }

    @Override
    public void onSettingConfirmed(int startingLife, int nbOfPlayers) {
        emptyListFragment();

        Intent lifeCounterIntent = getIntent();

        Bundle args = new Bundle();
        args.putInt("startingLife", startingLife);
        args.putInt("nbOfPlayers", nbOfPlayers);

        lifeCounterIntent.putExtras(args);
        finish();
        startActivity(lifeCounterIntent);
    }

    protected void emptyListFragment(){
        for(int i = 0; i < numberOfPlayers; i++){
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.remove(lifeTotalFragmentList.get(i));
            ft.commit();
        }
    }

    protected void resetLifeTotals(View v){
        emptyListFragment();
        this.recreate();
    }
}
