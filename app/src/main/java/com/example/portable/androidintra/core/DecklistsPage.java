package com.example.portable.androidintra.core;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.portable.androidintra.Fragments.NewDeckDialog;
import com.example.portable.androidintra.R;
import com.example.portable.androidintra.bean.Deck;
import com.example.portable.androidintra.interfaces.NewDeckFragmentInterface;
import com.example.portable.androidintra.utils.DAO;
import com.example.portable.androidintra.adapters.DeckListAdapter;

import java.util.List;

/**
 * Created by Vlad on 11/17/2016.
 */

public class DecklistsPage extends AppCompatActivity implements NewDeckFragmentInterface {
    private DAO dao;
    private DeckListAdapter deckListAdapter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.decklists);

        dao = new DAO(this);

        List<Deck> decks = dao.loadAllDecks();
        ListView listView = (ListView) findViewById(R.id.deckList);
        deckListAdapter = new DeckListAdapter(this, decks);
        listView.setAdapter(deckListAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.decklists_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.newDeck:
                createNewDeck();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void createNewDeck(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        NewDeckDialog dialog = new NewDeckDialog();
        dialog.attach(this);
        dialog.show(fragmentManager, "DialogSettings");
    }
    @Override
    public void onSettingConfirmed(String deckName) {
        int newDeckId = findNextDeckId();
        dao.insertNewDeck(newDeckId, deckName);
        deckListAdapter.updateList(dao.loadAllDecks());
    }
    private int findNextDeckId(){
        List<Integer> currentIds = dao.getDeckIds();
        for(int i = 0; i < currentIds.size(); i++){
            if(!currentIds.contains(i)){
               return i;
            }
        }
        return currentIds.size()+1;
    }
    public void modifyDeck(View v){
        int deckId = Integer.valueOf(v.getTag().toString());
        Deck deck = dao.loadDeckById(deckId);

        Intent intent = new Intent(this, DeckDetails.class);
        intent.putExtra("deck", deck);
        startActivity(intent);
    };

    public void deleteDeck(View v){
        final View view = (TextView) v;
        final int deckId = Integer.valueOf(view.getTag().toString());
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage("Are you sure you want delete this deck?");
        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dao.deleteDeckById(deckId);
                deckListAdapter.updateList(dao.loadAllDecks());
            }
        });

        dialog.show();
    }
}
